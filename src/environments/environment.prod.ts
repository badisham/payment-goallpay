export const environment = {
  production: true,
  title: 'Production Environment Heading',
  apiBaseUrl: 'https://api.uppatch-01.com/api',
};
