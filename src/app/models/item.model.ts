export interface IItem {
    id: string;
    name: string;
    price: number;
    fee?: number;
    qty?: number;
    imageUrl?: string;
    isDeleted?: boolean;
    isEnable?: boolean;
    createdAt: Date;
    updatedAt: Date;
}

export interface IInventory {
    id: string;
    items: IItemInventory[];
    createdAt: Date;
    updatedAt: Date;
}

export interface IItemInventory {
    id: string;
    name: string;
    qty: number;
    inventoryID?: string;
    status?: string;
    price?: number;
    isEnable?: boolean;
    isDeleted?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
    item?: IItem;
}