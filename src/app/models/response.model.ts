export interface IResponse {
    statusCode?: number;
    message?: string;
    result?: any;
    processing_time?: number;
}