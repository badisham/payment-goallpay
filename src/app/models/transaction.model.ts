import { IInventory, IItem } from './item.model';
import { IUser } from './user.model';

export interface ITrans {
  id: string;
  dBillID: string;
  tranType: string;
  saleOrders: ISaleOrders[];
  movingItems: IMovingItems[];
  paymentTrans: IPaymentTrans[];
  createByUser?: IUser;
  createByUserAt?: Date;
  status: string;
  isSuccess?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface ISaleOrders{
  id: string;
  item: IItem;
  qty: number;
  amount: number;
  fee: number;
  lastPrice: number;
}

export interface IMovingItems{
  movingType: string;
  qty: number;
  inventory: IInventory; 
}

export interface IPaymentTrans{
  paymentChannel: string;
  amount: number;
  currency: string;
  paymentTranRefID: string;
  paymentTranRef2ID: string;
  paymentTranResponse: string;
  paymentTranEPaymentCode: string;
  paymentTranServerGenID: string;
}