export interface IUser {
  id: string;
  username: string;
  password?: string;
  imageUrl?: string;
  isDeleted?: boolean;
  isEnable?: boolean;
  role?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IToken {
  token: string;
  refresh_token: string;
}