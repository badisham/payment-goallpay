export const minString = (str: string, len: number) => {
  const strLen = str.length || 0;
  const resStr = str.slice(0, len) + '...' + str.slice(strLen - 5, strLen);
  return resStr;
};

export const getDefaultImg = () => {
  return '/assets/images/default-image.png';
}


export const getDatePrefix = () => {
  function pad2(n:number) { return n < 10 ? '0' + n : n }
  var date = new Date();
  return (date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2(date.getDate()) + pad2(date.getHours()) + pad2(date.getMinutes()) + pad2(date.getSeconds()));
}
