import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { CrudService, T } from './crud.service';
import { IToken, IUser } from '../models/user.model';
import { IResponse } from '../models/response.model'
import { PageControllerService } from './page-controller.service';
import { catchError, map, takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthorizeService {
  public isLogin$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );

  public token$: BehaviorSubject<IToken | null> =
    new BehaviorSubject<IToken | null>(null);
    
  public user$: BehaviorSubject<IUser | null> =
  new BehaviorSubject<IUser | null>(null);

  constructor(
    private crudService: CrudService,
    private pageService: PageControllerService
  ) { }

  public login(username: string, password: string): Observable<IToken>{
    this.pageService.isLoading$.next(true);
    return this.crudService
    .post<IToken>('/auth/login', {
      username: username,
      password: password,
    })
    .pipe(
      map((res) => {
        
        this.token$.next(res);
        localStorage.setItem('token', res.token);
        localStorage.setItem('refresh_token', res.refresh_token);
        this.pageService.isLoading$.next(false);
        const user = this.parseJwt(res.token) as IUser;
        this.user$.next(user);
        return res as IToken;
      }),
      catchError((error) => {
        this.pageService.isLoading$.next(false);
        return throwError(error);
      })
    );
  }

  public register(
    username: string,
    password: string,
    email: string,
    imageUrl: string,
  ): any {
    this.pageService.isLoading$.next(true);
    return this.crudService.post<IUser>('/auth/register', {
      username: username,
      password: password,
      email: email,
      imageUrl: imageUrl
    });
  }

  public logout() {
    localStorage.removeItem('token');
    this.token$.next(null);
    this.isLogin$.next(false);
  }

  // public getUserByToken(): Observable<IUser> {
  //   this.pageService.isLoading$.next(true);
  //   return this.crudService.post<IUser>('/auth/get-user-by-token').pipe(
  //     map((res: IUser) => {
  //       if (res.accessToken) {
  //         this.setToken(res.accessToken);
  //       }
  //       this.token$.next(res);
  //       this.pageService.isLoading$.next(false);
  //       return res as IUser;
  //     }),
  //     catchError((error) => {
  //       this.pageService.isLoading$.next(false);
  //       return throwError(error);
  //     })
  //   );
  // }

  // public setToken(token: string): void {
  //   localStorage.setItem('token', token);
  // }

  public parseJwt(token: string) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  };
}
