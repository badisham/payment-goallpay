import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { IUser } from 'app/models/user.model';
import { AuthorizeService } from './authorize.service';

@Injectable({
  providedIn: 'root',
})
export class ManagerGuardGuard implements CanActivate {
  constructor(public auth: AuthorizeService, public router: Router) {}

  canActivate(): Promise<boolean> {
    return new Promise((resolve) => {
      this.auth.user$.subscribe((user) => {
        const isManager = user?.role === 'ADMIN';
        resolve(isManager);
      });
    });
  }
}
