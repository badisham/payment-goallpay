import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthorizeService } from './authorize.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardGuard implements CanActivate {
  constructor(public auth: AuthorizeService, public router: Router) {}

  async canActivate(): Promise<boolean> {
    let token = localStorage.getItem('token');
    if (!token || token === '') {
      this.router.navigate(['login'], { replaceUrl: true });
      return false;
    }
    return true;
  }
}
