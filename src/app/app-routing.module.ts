import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SiteLayoutComponent } from './layouts/site-layout/site-layout.component';
import { ForgotPasswordComponent } from './pages/auth/forgot-password/forgot-password.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { ResetPasswordComponent } from './pages/auth/reset-password/reset-password.component';
import { HomeComponent } from './pages/home/home.component';
import { InventoryComponent } from './pages/inventory/inventory.component';
import { RecieveCheckoutComponent } from './pages/recieve-checkout/recieve-checkout.component';
import { ShopComponent } from './pages/shop/shop.component';
import { TransactionComponent } from './pages/transaction/transaction.component';
import { UserDetailComponent } from './pages/user-detail/user-detail.component';
import { AuthGuardGuard } from './services/auth-guard.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'receive',
    component: RecieveCheckoutComponent,
  },
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      {
        path: '',
        pathMatch: 'prefix',
        redirectTo: 'home'
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            component: HomeComponent
          },
        ]
      },
      {
        path: 'user/:id',
        component: UserDetailComponent
      },
      // {
      //   path: 'shop',
      //   component: ShopComponent,
      // },
      {
        path: 'inventory',
        component: InventoryComponent,
      },
      {
        path: 'transaction',
        component: TransactionComponent,
      },
    ],
    canActivate: [AuthGuardGuard],
  },
  {
    path: '**',
    redirectTo: 'login',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
