import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IUser } from 'app/models/user.model';
import { AuthorizeService } from 'app/services/authorize.service';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { environment } from 'environments/environment';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { IItem } from 'app/models/item.model';
import { getDatePrefix } from 'app/engine/extension';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export class ListItem {
  items: IItem[] = [];
  setItem(i: IItem[]): void {
    this.items = i;
  }
}

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public userId: string = '';
  public isOwner: boolean = false;
  public isLoading: boolean = true;

  public isAdmin: boolean = false;
  public user: IUser = {} as IUser;
  public items: IItem[] = [];
  public itemsPrice: any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private pageService: PageControllerService,
    private crud: CrudService,
    private _bottomSheet: MatBottomSheet,
    private listItem: ListItem,
    private dialog: MatDialog,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.pageService.isMainpage$.next(false);
    this.isLoading = false;
    this.userId = this.activatedRoute.snapshot.paramMap.get('id') || '';
    if(!this.userId){
      this.router.navigate([`home`]);
      return;
    }
    // this.checkAdmin();
    this.crud
      .get<any>(`/user/${this.userId}`)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        (result) => {
          this.user = result as IUser;
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );

    this.crud
      .get<any>(`/shopItem`, {
        limit: 99,
        page: 1,
        search: '',
        sort: 'createdAt',
        direction: 'DESC'
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        (result) => {
          this.items = result.data as IItem[];
          this.items.forEach(i => {
            if(i.name === 'test1'){
              i.imageUrl = '/assets/images/s1.png';
            }else if(i.name === 'test2'){
              i.imageUrl = '/assets/images/s2.png';
            }else{
              i.imageUrl = '/assets/images/s2.png';
            }
          });
          this.items.forEach(i => {
            this.itemsPrice[i.id] = i.price;
          });
          this.listItem.setItem(this.items);
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }
  
  public openBottomSheet(): void {
    this.listItem.items = this.items || [];
    const bottomSheetRef = this._bottomSheet.open(BottomSheetOverviewExampleSheet);
  
    bottomSheetRef.afterDismissed().subscribe(res => {
      if(res){
        this.openDialog(res);
      }
    })
    
  }

  openDialog(id: string) {
    const dialogRef = this.dialog.open(ModalSelectPayment, {
      width: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.selectItemBuy(id, result);
    });
  }

  
  selectItemBuy(id: string, paymentChannel: string): void {
    Swal.fire({
      title: `Are you sure purchase ${this.itemsPrice[id]} CNY`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3f51b5',
      cancelButtonColor: '#ccc',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.isConfirmed) {
        this.purchase(id,paymentChannel);
      }
    })
  }

  purchase(id: string, paymentChannel: string): void {
    this.crud
      .post<any>(`/payment/request-epay`, {
        refID: getDatePrefix(),
        itemID: id,
        qty: 1,
        paymentMethod: paymentChannel,
        targetID: this.userId
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        (res) => {
          const payUrl = res.payUrl;
          if (payUrl){
            window.location.href = payUrl;
          }
          else {
            Swal.fire({
              icon: 'error',
              text: 'Something wrong',
            });
          }
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }
}

@Component({
  selector: 'bottom-sheet-overview-example-sheet',
  templateUrl: 'bottom-sheet-overview-example-sheet.html',
  styleUrls: ['./bottom-sheet-overview-example-sheet.scss']
})
export class BottomSheetOverviewExampleSheet{
  constructor(
    private _bottomSheetRef: MatBottomSheetRef<BottomSheetOverviewExampleSheet>,
    private listItem: ListItem,
    public dialog: MatDialog) { }

  public items: IItem[] = this.listItem.items;

  selectItem(id: string):void {
    this._bottomSheetRef.dismiss(id);
  }
}

@Component({
  selector: 'select-paymentchannel-dialog',
  templateUrl: 'select-paymentchannel-dialog.html',
  styleUrls: ['./select-paymentchannel-dialog.scss']
})
export class ModalSelectPayment {
  constructor(public dialogRef: MatDialogRef<ModalSelectPayment>) { }
    
    selectChannel(v: string): void{
      this.dialogRef.close(v);
    }
}