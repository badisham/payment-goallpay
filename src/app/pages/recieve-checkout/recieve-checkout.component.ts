import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { getDatePrefix } from 'app/engine/extension';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-recieve-checkout',
  templateUrl: './recieve-checkout.component.html',
  styleUrls: ['./recieve-checkout.component.scss']
})
export class RecieveCheckoutComponent implements OnInit, OnDestroy {

  private unsubscribeAll: Subject<any> = new Subject();
  public filter$!: Observable<any>;
  constructor(
    private route: ActivatedRoute,
    private crud: CrudService,
    private router: Router,
    private pageService: PageControllerService

  ) { }
  isLoading = true;
  isError = false;
  isSuccess = false;

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  b64DecodeUnicode(str: any) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }

  ngOnInit(): void {
    this.filter$ = this.route.queryParamMap.pipe(
      map((params: ParamMap) => {
        let paymentChannel = this.paymentChannelSchema(`${params.get('paymentSchema')}`);
        return {
          params: JSON.parse(this.b64DecodeUnicode(params.get('callback'))),
          RespCode: params.get('RespCode'),
          ref2ID: params.get('transID'),
          orderNum: params.get('orderNum'),
          paymentChannel: paymentChannel
        }
      }),
    );
    this.filter$.subscribe(param => {
      if (param !== null) {
        // console.log('param',param);
        // // this.dbillID = param;
        // let str = JSON.parse(this.b64DecodeUnicode(param.callback))

        // console.log('params',str);
        if (param.RespCode == '00') {
          this.checkout(param);
        } else {
          this.isLoading = false;
          this.isError = true;
        }

      }
    });
  }

  paymentChannelSchema(paymentChannel: string) {
    switch (paymentChannel) {
      case 'UP':
        return 'UNIONPAY'
        break;
      case 'FC':
        return 'FC'
        break;
      case 'WX':
        return 'WECHAT'
        break;
      case 'AP':
        return 'ALIPAY'
        break;
      case 'PAYPAL':
        return 'PAYPAL'
        break;
      case 'APP':
        return 'APPLE'
        break;
      default:
        return null;
        break;
    }
  }

  checkout(data: any): void {
    this.crud
      .post<any>(`/order`, {
        tranType: 'TRANS_GIVE',
        targetID: data.params.targetID,
        items: [
          {
            itemID: data.params.itemInfo.id,
            qty: 1,
          }
        ],
        payment: {
          paymentChannel: data.paymentChannel,
          amount: data.params.itemInfo.price,
          currency: 'CNY',
          paymentTranRefID: data.params.refID,
          paymentTranRef2ID: data.ref2ID,
          paymentTranResponse: 'success',
          paymentTranEPaymentCode: '',
          paymentTranServerGenID: '',
        }
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        (result) => {
          this.isLoading = false;
          this.isSuccess = true;
          if (result.isSuccess) {
            setTimeout(() => {
              this.router.navigate([`home`]);
                this.router.navigate([`home`]);
            }, 3000);
          } else {
            setTimeout(() => {
              this.router.navigate([`/user/${data.params.targetID}`]);
            }, 3000);
          }

          // setTimeout(() => {
          //   if (result.isSuccess) {
          //     this.router.navigate([`home`]);
          //   } else {
          //     this.router.navigate([`/user/${data.params.targetID}`]);
          //   }
          // }, 3000);
        },
        (error) => {
          this.isLoading = false;
          this.isError = true;
          // Swal.fire({
          //   icon: 'error',
          //   text: error.message,
          // });
          // this.isLoading = false;
          setTimeout(() => {
            this.router.navigate([`/user/${data.params.targetID}`]);
          // this.router.navigate([`/user/${data.params.targetID}`]);
          }, 3000);
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }

}

// http://localhost:4200/receive?callback=eyJyZWZJRCI6IjIwMjIwNTE0MjE0NzIxIiwic3VjY2VzcyI6dHJ1ZX0%3D
// GWTime=20220514195345
// RespCode=00
// RespMsg=success
// acqID=99020344
// charSet=UTF-8
// merID=600039253112292
// merReserve=
// orderAmount=1
// orderCurrency=CNY
// orderNum=20220514214721
// paymentSchema=UP
// signType=MD5
// signature=a94f767ba4a8e4319854130cbc632da0
// transID=QV4xLbsLvYA8QEkb
// transTime=20211230114713
// transType=PURC
// version=VER000000005