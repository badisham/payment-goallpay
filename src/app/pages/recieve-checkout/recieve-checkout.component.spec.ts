import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecieveCheckoutComponent } from './recieve-checkout.component';

describe('RecieveCheckoutComponent', () => {
  let component: RecieveCheckoutComponent;
  let fixture: ComponentFixture<RecieveCheckoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecieveCheckoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecieveCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
