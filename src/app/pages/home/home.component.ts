import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IUser } from 'app/models/user.model';
import { AuthorizeService } from 'app/services/authorize.service';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public isLoading: boolean = false;
  public users: IUser[] = [];

  constructor(
    private router: Router,
    private pageService: PageControllerService,
    private crud: CrudService,
    private auth: AuthorizeService
    ) {
  }
  ngOnInit(): void {
    this.isLoading = true;
    this.pageService.isMainpage$.next(false);

    this.isLoading = false;
    
    this.crud
      .get<any>(`/user`, {
        limit: 99,
        page: 1,
        search: '',
        sort: 'createdAt',
        direction: 'DESC'
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        (result) => {
          this.users = result.data as IUser[];
          this.isLoading = false;
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }

  openDetail(id: string): void {
    this.router.navigate([`/user/${id}`]);
  }
}
