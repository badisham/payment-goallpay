import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ITrans } from 'app/models/transaction.model';
import { AuthorizeService } from 'app/services/authorize.service';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

export interface TranItem {
  id: string;
  name: string;
  price: number;
  qty: number;
}

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})

export class TransactionComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  constructor(
    private auth: AuthorizeService,
    private router: Router,
    private crud: CrudService,
    private pageService: PageControllerService
  ) { }

  public searchText: string = '';
  public trans: ITrans[] = [];
  public filterTrans: any[] = [];

  displayedColumns: string[] = ['name', 'qty', 'price'];
  public dataSource: TranItem[] = [];

  ngOnInit(): void {
    this.auth.token$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((res) => res?.token);
    this.pageService.isMainpage$.next(true);
    this.crud
      .get<ITrans[]>(`/order/me`, {
        limit: 99,
        page: 1,
        search: '',
        sort: 'createdAt',
        direction: 'DESC'
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        async (res: any) => {
          this.trans = res.data as ITrans[];
          this.filterTransaction();
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }

  increase(total: number, num: number): number {
    return total + num;
  }

  filterTransaction(): void {
    if (this.trans.length > 0) {
      this.dataSource = this.trans
        .filter((tran) => {
          const search = this.searchText.toLowerCase();

          const price = tran.saleOrders[0].amount * tran.saleOrders[0].qty;
          return (
            price.toString().includes(search) ||
            tran.saleOrders[0].item?.name?.toLowerCase().includes(search)
          );
        })
        .map((tran) => {
          const price = tran.saleOrders[0].amount * tran.saleOrders[0].qty;
          return {
            id: tran.id,
            name: tran.saleOrders[0].item?.name,
            price: price,
            qty: tran.saleOrders[0].qty
          };
        });
    }
  }
}
