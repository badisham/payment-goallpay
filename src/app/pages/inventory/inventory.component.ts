import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IInventory, IItemInventory } from 'app/models/item.model';
import { AuthorizeService } from 'app/services/authorize.service';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

export interface InvenItem {
  id: string;
  name: string;
  qty: number;
}

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  constructor(
    private crud: CrudService,
    private pageService: PageControllerService
  ) { }

  public searchText: string = '';
  public invenItems: IInventory[] = [];
  public filterinvenItems:any[] = [];
  public isAdmin: boolean = false;

  displayedColumns: string[] = ['name', 'qty'];
  public dataSource:InvenItem[]= [];
  

  ngOnInit(): void {
    this.pageService.isMainpage$.next(true);
    this.crud
      .get<IInventory[]>(`/inventory/me`, {
        limit: 99,
        page: 1,
        search: '',
        sort: 'createdAt',
        direction: 'DESC'
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        async (res: any) => {
          this.invenItems = res.data as IInventory[];
          console.log(this.invenItems);
          
          this.filterinvenItem();
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }

  filterinvenItem(): void {
    if (this.invenItems[0].items.length > 0) {
      this.dataSource = this.invenItems[0].items
        .filter((item) => {
          const search = this.searchText.toLowerCase();
          return (
            item.qty?.toString().includes(search) ||
            item.item?.name.toLowerCase().includes(search)
          );
        })
        .map((item) => {
          return {
            id: item.id,
            name: item.item?.name || '',
            qty: item.qty || 0,
            // price: item.item?.price || 0,
          };
        });
    }
  }
}
