import { Component, OnDestroy, OnInit } from '@angular/core';
import { IItem } from 'app/models/item.model';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import * as md5 from 'md5';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit , OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  constructor(private pageService: PageControllerService,
    private crud: CrudService) { }

  public items: IItem[] = []
  ngOnInit(): void {
    this.pageService.isMainpage$.next(true);
    this.crud
      .get<IItem[]>(`/shopItem`, {
        limit: 99,
        page: 1,
        search: '',
        sort: 'createdAt',
        direction: 'DESC'
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        async (res: any) => {
          this.items = res.data as IItem[];
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }

  selectItemBuy(id: string): void{
    Swal.fire({
      title: 'Are you sure purchase this ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3f51b5',
      cancelButtonColor: '#ccc',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.isConfirmed) {
        
        // TODO: Purchase 
      }
    })
  }
}
