import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthorizeService } from 'app/services/authorize.service';
import { CrudService } from 'app/services/crud.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { PageControllerService } from 'app/services/page-controller.service';
import { IToken } from 'app/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public username: string = localStorage.getItem('email') || 'test2';
  public password: string = 'Dragonquest9';
  public isLoading: boolean = false;
  public isRemember: boolean = false;
  public hide: boolean = true;

  constructor(
    private crud: CrudService,
    private auth: AuthorizeService,
    private route: ActivatedRoute,
    private router: Router,
    private pageService: PageControllerService
  ) { }

  async ngOnInit(): Promise<void> {
    this.pageService.isLoading$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((res) => {
        this.isLoading = res;
      });
    // this.auth.token$.pipe(takeUntil(this.unsubscribeAll)).subscribe((res) => {
    //   this.pageService.isLoading$.next(false);
    //   this.router.navigate(['user'], { replaceUrl: true });
    //   // this.router.navigate(['m/user'], { relativeTo: this.route });
    // });
    // const token_ = localStorage.getItem('token');
    // if (token_ !== '') {
    //   const token = {
    //     token: token_,
    //     refresh_token: localStorage.getItem('refresh_token')
    //   } as IToken;
    //   this.auth.token$.next(token)
    //   this.auth.token$.subscribe((res) => {
    //     if (res)
    //       this.router.navigate(['home'], { replaceUrl: true });
    //   })
    // }
  }

  public login(): void {
    if (!this.username || !this.password) {
      Swal.fire({
        icon: 'warning',
        text: 'Please input username and password',
      });
      return;
    }

    if (!this.isRemember) {
      localStorage.removeItem('username');
    }
    this.auth
      .login(this.username, this.password)
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        (res) => {
          if (this.isRemember) {
            localStorage.setItem('username', this.username);
          }
          this.router.navigate(['home']);
          // this.router.navigate(['m/user'], { relativeTo: this.route });
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }

  setFocus(id: string) {
    const ele = document.getElementById(id);
    ele?.focus();
  }
}
