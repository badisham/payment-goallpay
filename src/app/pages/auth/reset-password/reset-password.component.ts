import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizeService } from 'app/services/authorize.service';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  private key: string = '';
  public password: string = '';
  public c_password: string = '';
  public isLoading: boolean = false;

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }
  constructor(
    private activatedRoute: ActivatedRoute,
    private crud: CrudService,
    private pageController: PageControllerService,
    private router: Router
  ) {
    this.key = this.activatedRoute.snapshot.paramMap.get('key') || '';
  }

  ngOnInit(): void {}

  setFocus(id: string) {
    const ele = document.getElementById(id);
    ele?.focus();
  }

  resetPassword() {
    if (!this.password || !this.c_password) {
      return;
    }
    if (this.password !== this.c_password) {
      Swal.fire({
        icon: 'warning',
        text: 'Password not same',
      });
      return;
    }

    const keys = this.key.split('--');
    const userId = keys[0];
    const repassKey = keys[1];

    this.crud
      .post(`/auth/reset-pass`, {
        userId,
        repassKey,
        password: this.password,
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        async (res: any) => {
          Swal.fire({
            icon: 'success',
            text: res,
          });
          this.router.navigate(['user']);
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageController.isMainpage$.next(false);
        }
      );
  }
}
