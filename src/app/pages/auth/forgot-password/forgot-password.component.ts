import { Component, OnDestroy, OnInit } from '@angular/core';
import { CrudService } from 'app/services/crud.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  public isLoading: boolean = false;
  public email: string = '';

  constructor(
    private crud: CrudService,
    private pageController: PageControllerService
  ) {}

  ngOnInit(): void {}

  sendResetPassword() {
    if (!this.email) {
      Swal.fire({
        icon: 'warning',
        text: 'Please input email',
      });
      return;
    }
    this.crud
      .post(`/auth/reset-pass-mail`, {
        email: this.email,
      })
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        async (res: any) => {
          Swal.fire({
            icon: 'success',
            text: res,
          });
        },
        (error) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageController.isMainpage$.next(false);
        }
      );
  }
}
