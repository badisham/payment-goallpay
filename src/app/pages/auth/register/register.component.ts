import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizeService } from 'app/services/authorize.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();

  public username: string = '';
  public password: string = '';
  public email: string = '';
  public imageUrl: string = '';
  public password_confirm: string = '';
  public isLoading: boolean = false;

  constructor(
    // private route: ActivatedRoute,
    private router: Router,
    private auth: AuthorizeService,
    private pageService: PageControllerService
  ) {
    // redirect to home if already logged in
    // if (this.authorizeService.currentUserValue) {
    //   this.router.navigate(['/']);
    // }
  }
  ngOnInit(): void {
    this.pageService.isLoading$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((res) => {
        this.isLoading = res;
      });
  }
  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }

  async register(): Promise<void> {
    if (this.password != this.password_confirm) {
      Swal.fire('Password not same.');
      return;
    }
    if (!this.username || !this.password || !this.email || !this.imageUrl) {
      Swal.fire('Please input.');
      return;
    }
    this.auth
      .register(
        this.username,
        this.password,
        this.email,
        this.imageUrl
      )
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(
        (res: any) => {
          Swal.fire(res.message);
          // this.auth.setToken(res.accessToken);
          // this.auth.token$.next(res);
          this.router.navigate(['/login']);
        },
        (error: any) => {
          Swal.fire({
            icon: 'error',
            text: error.message,
          });
        },
        () => {
          this.pageService.isLoading$.next(false);
        }
      );
  }
}
