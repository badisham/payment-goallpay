import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthorizeService } from 'app/services/authorize.service';
import { PageControllerService } from 'app/services/page-controller.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CrudService } from 'app/services/crud.service';
import { environment } from 'environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-site-layout',
  templateUrl: './site-layout.component.html',
  styleUrls: ['./site-layout.component.scss'],
})
export class SiteLayoutComponent implements OnInit, OnDestroy {
  private unsubscribeAll: Subject<any> = new Subject();

  constructor(
    private router: Router,
    private auth: AuthorizeService,
    private crud: CrudService,
    private location: Location,
    public pageService: PageControllerService
  ) {}
  showFiller = false;
  public isMenu = false;
  public isMainPage = true;
  public isManager = false;
  public isPendingBox = false;
  public localUserId: string = '';
  public isLoading: boolean = false;

  ngOnInit(): void {
    this.pageService.isLoading$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((isLoading) => {
        setTimeout(() => {
          this.isLoading = isLoading;
        }, 10);
      });
    this.auth.token$.pipe(takeUntil(this.unsubscribeAll)).subscribe((user) => {
      // this.localUserId = user?.ID || '';
      // this.isManager = user === 'ADMIN';
    });
    this.pageService.isMainpage$
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe((isMain) => {
        setTimeout(() => {
          this.isMainPage = isMain;
        }, 10);
      });
  }

  checkPending(id: string): void {
    if (id) {
      this.crud
        .get<boolean>(`/trans/${id}`)
        .pipe(takeUntil(this.unsubscribeAll))
        .subscribe(
          (res) => {
            this.isPendingBox = res;
          },
          (error) => {
            Swal.fire('Server fail.');
            console.log(error);
          },
          () => {
            this.pageService.isLoading$.next(false);

          }
        );
    }
  }

  openMenu(): void {
    this.isMenu = !this.isMenu;
  }
  logout(): void {
    this.auth.logout();
    this.router.navigate(['login']);
  }

  gotoback(): void {
    this.location.back();
  }

  ngOnDestroy(): void {
    this.unsubscribeAll.next();
    this.unsubscribeAll.complete();
  }
}
